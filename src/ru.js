import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
  ru: {
    home: {
      title: 'Список постов',
      noPosts: 'Нет постов',
      delete: 'Удалить',
      createPost: 'Создать новый пост',
    },
    createPost: {
      title: 'Создание нового поста',
      nameLabel: 'Название:',
      descriptionLabel: 'Описание:',
      tagLabel: 'Тег {index}:',
      removeTag: 'Удалить тег',
      addTag: 'Добавить еще один тег',
      save: 'Сохранить',
    },
  }
};

const i18n = new VueI18n({
  locale: 'ru', 
  messages,
});

export default i18n;

