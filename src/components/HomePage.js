import axios from 'axios';


export default {
  computed: {
    posts() {
      return this.$store.getters.posts;
    }
  },
  mounted() {
    if (this.posts.length === 0) {
      axios.get('/posts.json')
        .then((response) => {
          this.$store.dispatch('setPosts', response.data.posts);
        })
        .catch((error) => {
          console.error(error);
          alert('Ошибка');
        });
    }
  },
  methods: {
    deletePost(id) {
      axios.delete(`http://localhost:8080/${id}`)
        .finally(() => {
          this.$store.commit('deletePost', id);
        });
    },
  },
};

