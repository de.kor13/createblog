import axios from 'axios';



export default {
  data() {
    return {
      name: '',
      description: '',
      tags: [{ id: 1, name: '' }],
      maxNameLength: 255,
      maxDescriptionLength: 4096,
      maxTags: 10
    };
  },
  methods: {
    addTag() {
      if (this.tags.length < this.maxTags) {    
      const newId = this.tags.length + 1;
      this.tags.push({ id: newId, name: '' });
     }
    },
    removeTag(index) {
      this.tags.splice(index, 1);
    },
    createPost() {

      const tags = this.tags.filter((tag) => tag.name !== '').map((tag) => ({ name: tag.name }));
      const newPost = {
        id: Math.floor(Math.random() * 1000000), 
        name: this.name,
        description: this.description,
        tags: tags.length ? tags : null,
      };
    
      axios.post('example.com/api/posts', { post: newPost })
        .finally(() => {
          this.$store.dispatch('addPost', newPost);
          this.$router.push('/');
        })
  
    },
  },
};




