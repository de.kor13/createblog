import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [],
  },
  actions: {
    addPost({ commit }, post) {
      commit('addPost', post);
    },
    setPosts({ commit }, posts) {
      commit('setPosts', posts);
    },
    deletePost({ commit }, id) {
      commit('deletePost', id);
    }
  },
  mutations: {
    addPost(state, post) {
      state.posts.push(post);
    },
    setPosts(state, posts) {
      state.posts = posts;
    },
    deletePost(state, id) {
      state.posts = state.posts.filter(post => post.id !== id);
    }
  },
  getters: {
    posts: state => state.posts,
  },
});
