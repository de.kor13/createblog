import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import HomePage from './components/HomePage.vue';
import CreatePost from './components/CreatePost.vue';
import store from './store';
import i18n from './ru.js';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: '/', component: HomePage },
    { path: '/create-post', component: CreatePost },
  ],
});

new Vue({
  router,
  store,
  i18n, 
  render: (h) => h(App),
}).$mount('#app');

